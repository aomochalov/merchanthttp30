package com.http30;

import com.http30.sockets.SocketProcessor;

import java.net.ServerSocket;
import java.net.Socket;

public class Main implements ILogger {

    public static void main(String[] args) throws Throwable {
        ServerSocket ss = new ServerSocket(8190);
        while (true) {
            Socket s = ss.accept();
            LOGGER.info("Socket open on port 8190 : " + s);
            // ��� ���
            new Thread(new SocketProcessor(s)).start();
            // ��� �����
            //new Thread(new TestSocketProcessor(s)).start();
        }
    }
}
