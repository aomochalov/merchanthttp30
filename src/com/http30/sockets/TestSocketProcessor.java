package com.http30.sockets;

import com.http30.sockets.AbstactSocket;

import java.io.IOException;
import java.net.Socket;

public class TestSocketProcessor extends AbstactSocket {
    public TestSocketProcessor(Socket s) throws IOException {
        super(s);
    }

    @Override
    public void run() {
        try {
            String requestString =
                    "POST / HTTP/1.1\n" +
                            "User-Agent: Yandex.Money\n" +
                            "Content-Length: 674\n" +
                            "Content-Type: application/x-www-form-urlencoded; charset=UTF-8\n" +
                            "Host: ec2-52-29-4-93.eu-central-1.compute.amazonaws.com:8190\n" +
                            "Connection: Keep-Alive\n" +
                            "Accept-Encoding: gzip,deflate\n" +
                            "\n" +
                            "orderSumAmount=243.00&paymentPayerCode=4100322772210&cps_rebillingAllowed=false&isViaWeb=true&paymentType=PC&requestDatetime=2015-09-18T10%3A51%3A06.556%2B03%3A00&nst_unilabel=1d8dd620-0009-5000-8000-000001639fb4&cps_user_country_code=RU&orderCreatedDatetime=2015-09-18T10%3A50%3A58.752%2B03%3A00&action=checkOrder&shopId=19&isOUTshop=true&scid=61914&shopSumBankPaycash=1003&shopSumCurrencyPaycash=643&rebillingOn=false&orderSumBankPaycash=1003&orderSumCurrencyPaycash=643&merchant_order_id=fgh_180915105056_00000_19&successURL=&customerNumber=fgh&targetcurrency=643&payment-name=DEMO+HTTP+30&invoiceId=2000000590985&shopSumAmount=230.85&md5=60FDBD679CB47B230B4304B532D35F30Request{requestDatetime=2015-09-18T10%3A51%3A06.556%2B03%3A00, action='checkOrder', md5='60FDBD679CB47B230B4304B532D35F30', shopId=19, invoiceId=2000000590985, customerNumber='fgh', orderCreatedDatetime=2015-09-18T10%3A50%3A58.752%2B03%3A00, orderSumAmount='null', orderSumCurrencyPaycash='643', orderSumBankPaycash='1003', shopSumAmount='230.85', shopSumCurrencyPaycash='643', shopSumBankPaycash='1003', paymentPayerCode='4100322772210', paymentType='PC'}HTTP/1.1 200 OK\n";
            LOGGER.info("Request: " + requestString);
            writeResponse(getResponseString(requestString.split("\n")[8]));
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
        } finally {
            try {
                is.close();
                os.close();
                s.close();
            } catch (Throwable throwable) {
                LOGGER.error(throwable);
            }
        }
    }
}
