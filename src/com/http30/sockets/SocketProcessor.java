package com.http30.sockets;

import java.io.IOException;
import java.net.Socket;

public class SocketProcessor extends AbstactSocket {
    public SocketProcessor(Socket s) throws IOException {
        super(s);
    }

    @Override
    public void run() {
        try {
            String requestString = readInputData();
            LOGGER.info("Request: " + requestString);
            writeResponse(getResponseString(requestString.split("\n")[8]));
        } catch (Throwable throwable) {
            LOGGER.error(throwable);
        } finally {
            try {
                is.close();
                os.close();
                s.close();
            } catch (Throwable throwable) {
                LOGGER.error(throwable);
            }
        }
    }
}
