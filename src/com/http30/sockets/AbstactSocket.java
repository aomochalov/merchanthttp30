package com.http30.sockets;

import com.http30.ILogger;
import com.http30.request.Request;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

abstract class AbstactSocket implements Runnable, ILogger {
    Socket s;
    InputStream is;
    OutputStream os;

    public AbstactSocket(Socket s) throws IOException {
        this.s = s;
        this.is = s.getInputStream();
        this.os = s.getOutputStream();
    }

    void writeResponse(String s) throws Throwable {
        String response = "HTTP/1.1 200 OK\r\n" +
                "Server: " + InetAddress.getLocalHost().getHostName() + "\r\n" +
                "Content-Type: text/xml\r\n" +
                "Content-Length: " + s.length() + "\r\n" +
                "Connection: close\r\n\r\n";
        String result = response + s;
        os.write(result.getBytes());
        os.flush();
    }

    String readInputData() throws Throwable {
        Thread.sleep(500);
        byte[] bytes = new byte[is.available()];
        is.read(bytes);

        return new String(bytes);
    }


    String getResponseString(String stringRequest) throws IOException {
        Request request = new Request(stringRequest.split("&"));
        String stringResponse = request.getResponse();
        LOGGER.info("Response: " + stringResponse);
        return stringResponse;
    }
}
