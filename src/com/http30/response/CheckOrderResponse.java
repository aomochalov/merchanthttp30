package com.http30.response;

import org.jdom.*;

import java.io.IOException;

public class CheckOrderResponse extends ResponseAbstract {

    public CheckOrderResponse(long invoiceId, long shopId, int code) {
        super(invoiceId, shopId, code);
    }

    @Override
    public byte[] getResponseXMLDocument() throws IOException {
        Element root = new Element("checkOrderResponse");
        writeCommonResponseAttributes(root);
        return serialize(root);
    }
}
