package com.http30.response;

import org.jdom.Element;

import java.io.IOException;

public class PaymentAvisoRespone extends ResponseAbstract {

    public PaymentAvisoRespone(long invoiceId, long shopId, int code) {
        super(invoiceId, shopId, code);
    }

    @Override
    public byte[] getResponseXMLDocument() throws IOException {
        Element root = new Element("paymentAvisoResponse");
        writeCommonResponseAttributes(root);
        return serialize(root);
    }
}
