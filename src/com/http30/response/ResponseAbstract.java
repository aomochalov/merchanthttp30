package com.http30.response;


import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import org.jdom.*;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public abstract class ResponseAbstract {

    private int code;
    private long shopId;
    private long invoiceId;
    // ����������� ��������� ���� (UTC)
    private final TimeZone timeZone;

    public ResponseAbstract(long invoiceId, long shopId, int code) {
        this.invoiceId = invoiceId;
        this.shopId = shopId;
        this.code = code;
        timeZone = TimeZone.getTimeZone("UTC");
    }

    public abstract byte[] getResponseXMLDocument() throws IOException;

    void writeCommonResponseAttributes(Element root) {
        root.setAttribute("performedDatetime", formatISODateTime(System.currentTimeMillis()));
        root.setAttribute("code", Integer.toString(code));
        root.setAttribute("invoiceId", Long.toString(invoiceId));
        root.setAttribute("shopId", Long.toString(shopId));
    }

    private String formatISODateTime(long millis) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(millis);
        calendar.setTimeZone(timeZone);
        return new XMLGregorianCalendarImpl(calendar).toXMLFormat();
    }

    byte[] serialize(Element xmlRoot) throws IOException {
        Document doc = new Document(xmlRoot);
        XMLOutputter outputter = new XMLOutputter(Format.getCompactFormat());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        outputter.output(doc, out);
        out.close();
        return out.toByteArray();
    }
}
