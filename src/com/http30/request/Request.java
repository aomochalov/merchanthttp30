package com.http30.request;

import com.http30.Const;
import com.http30.response.CheckOrderResponse;
import com.http30.response.PaymentAvisoRespone;
import com.http30.response.ResponseAbstract;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Request implements Const {
    private String requestDatetime;
    private String action;
    private String md5;
    private long shopId;
    private long invoiceId;
    private String customerNumber;
    private String orderCreatedDatetime;
    private String orderSumAmount;
    private String orderSumCurrencyPaycash;
    private String orderSumBankPaycash;
    private String shopSumAmount;
    private String shopSumCurrencyPaycash;
    private String shopSumBankPaycash;
    private String paymentPayerCode;
    private String paymentType;

    public Request(String[] params) {
        for (String param : params) {
            String[] strings = param.split("=");
            String key = null;
            String value = null;
            if (strings.length >= 2) {
                key = strings[0];
                value = strings[1];
            }
            if (strings.length == 1) {
                key = strings[0];
                value = "";
            }
            assert key != null;
            switch (key) {
                case "requestDatetime":
                    requestDatetime = value;
                    break;
                case "action":
                    action = value;
                    break;
                case "md5":
                    md5 = value;
                    break;
                case "shopId":
                    shopId = new Long(value);
                    break;
                case "invoiceId":
                    invoiceId = new Long(value);
                    break;
                case "customerNumber":
                    customerNumber = value;
                    break;
                case "orderCreatedDatetime":
                    orderCreatedDatetime = value;
                    break;
                case "orderSumAmount":
                    orderSumAmount = value;
                    break;
                case "orderSumCurrencyPaycash":
                    orderSumCurrencyPaycash = value;
                    break;
                case "orderSumBankPaycash":
                    orderSumBankPaycash = value;
                    break;
                case "shopSumAmount":
                    shopSumAmount = value;
                    break;
                case "shopSumCurrencyPaycash":
                    shopSumCurrencyPaycash = value;
                    break;
                case "shopSumBankPaycash":
                    shopSumBankPaycash = value;
                    break;
                case "paymentPayerCode":
                    paymentPayerCode = value;
                    break;
                case "paymentType":
                    paymentType = value;
                    break;
            }
        }
    }

    @Override
    public String toString() {
        return "Request{" +
                "requestDatetime=" + requestDatetime +
                ", action='" + action + '\'' +
                ", md5='" + md5 + '\'' +
                ", shopId=" + shopId +
                ", invoiceId=" + invoiceId +
                ", customerNumber='" + customerNumber + '\'' +
                ", orderCreatedDatetime=" + orderCreatedDatetime +
                ", orderSumAmount='" + orderSumAmount + '\'' +
                ", orderSumCurrencyPaycash='" + orderSumCurrencyPaycash + '\'' +
                ", orderSumBankPaycash='" + orderSumBankPaycash + '\'' +
                ", shopSumAmount='" + shopSumAmount + '\'' +
                ", shopSumCurrencyPaycash='" + shopSumCurrencyPaycash + '\'' +
                ", shopSumBankPaycash='" + shopSumBankPaycash + '\'' +
                ", paymentPayerCode='" + paymentPayerCode + '\'' +
                ", paymentType='" + paymentType + '\'' +
                '}';
    }

    public String getResponse() throws IOException {
        ResponseAbstract response = null;
        int code = 0;
        if (md5.equals(getMd5Hash())) code = 0;
        else code = 1;
        if (action.equals("checkOrder"))
            response = new CheckOrderResponse(invoiceId, shopId, code);
        if (action.equals("paymentAviso"))
            response = new PaymentAvisoRespone(invoiceId, shopId, code);

        assert response != null;
        return new String(response.getResponseXMLDocument());
    }

    private String getMd5Hash() {
        MessageDigest messageDigest = null;
        byte[] digest = new byte[0];

        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            String string = action + ";" + orderSumAmount + ";" + orderSumCurrencyPaycash + ";" + orderSumBankPaycash + ";" + shopId + ";" + invoiceId + ";" + customerNumber + ";" + PASSWORD;
            messageDigest.update(string.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException ignored) {
        }

        BigInteger bigInt = new BigInteger(1, digest);
        String md5Hex = bigInt.toString(16);

        while (md5Hex.length() < 32) {
            md5Hex = "0" + md5Hex;
        }

        return md5Hex.toUpperCase();
    }
}
